# commands

kubectl get storageclass

# notes
kubernetes.io/no-provisioner = local-persistent-volumes

# local-persistent-volume vs hostPath volume

A hostPath volume mounts a file or directory from the host node's filesystem into your Pod. 
So, if you have a multi-node cluster, the pod is restarted for some reasons and assigned to another node, the new node won't have the old data on the same path. 
That's why we have seen, that hostPath volumes work well only on single-node clusters.

Here, the Kubernetes local persistent volumes help us to overcome the restriction and we can work in a multi-node environment with no problems. 
It remembers which node was used for provisioning the volume, thus making sure that a restarting POD always will find the data storage in the state it had left it before the reboot.

If a node storage dies completely and beyond repair (RAID usually prevents it), data of both hostpath and local persitent volumes of that node can be lost.

Ref:

    https://kubernetes.io/docs/concepts/storage/volumes/#hostpath
    https://vocon-it.com/2018/12/20/kubernetes-local-persistent-volumes/

