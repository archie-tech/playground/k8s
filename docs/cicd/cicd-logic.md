# CI/CD Logic

## Sample architecture

* Frontend: static s3 website
* Backend: k8s service (lb)
* Database: dynamodb

## Git directory structure

* service a
  * k8s manifests
  * code
* service b
  * k8s manifests
  * code

## Logic

test each service folder for changes, using the command

    git log --pretty=format:'%H' --max-count=1 /path/to/folder

for each changed service:

0. build
0. run unit tests
0. deliver images to ecr (make sure image versions in manifest are uniqe)

## Notes

* Changes in infrastructure are managed in another repository (e.g. terraform)
* Number of replicas are managed by auto-scaler
* How to handle changes in manifests that are not image version?
* How to handle manifests of a new service?
* Who or what is responsible for running kubectl apply?