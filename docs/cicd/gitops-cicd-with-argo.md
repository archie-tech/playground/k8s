# GitOps CI/CD with ArgoCD


## Sample architecture

* Frontend: static s3 website
* Backend: k8s service (lb)
* Database: dynamodb

## Git directory structure

* src
  * frontend
    * html
    * js
    * css
  * backend
    * k8s manifests
    * code

## Git branch names logic

* main branch
* feature / bug-fix branches

## CI Workflow

## CD Workflow

## Links

* https://codefresh.io/learn/gitops/