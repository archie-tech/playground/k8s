## Workflow

* Trigger: developer pushed feature branch
  * Actions: N/A
  * Side Effects:
  * Abort conditions:

* Trigger: merge request created
  * Actions
    * merge feature branch to main branch localy on ci server
    * run unit tests
    * define version as yyyy-mm-dd.commit-hash
    * tag container image (or rename lambda zip file)
    * push to ecr in test env (or to s3 bucket)
  * Side Effects:
  * Abort conditions:

