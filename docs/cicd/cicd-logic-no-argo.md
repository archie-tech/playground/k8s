# CI/CD Logic Without Argo CD

## Sample architecture

* Frontend: static s3 website
* Backend: k8s service (lb)
* Database: dynamodb

## Git directory structure

* src
  * frontend
    * html
    * js
    * css
  * backend
    * k8s manifests
    * code

## Git branch names logic

* main branch
* feature / bug-fix branches

## CI Workflow

trigger - change detection - actions

* push to feature branch 
  * backend code folder changed
    * build
    * run unit tests
    * create docker image
    * tag image (name = commit-hash/dev)
    * push to dev ecr
    * update version field of all images in manifest files to mach docker image tag
    * run kubectl apply on dev cluster
    * run end to end tests
  * frontend folder changed
    * build
    * run unit tests
    * copy files to s3 dev bucket
    * run end to end tests
* push to or merge into main branch
  * backend code folder changed
    * build
    * run unit tests
    * create docker image
    * tag image (name = commit-hash/rc)
    * push to production ecr
    * update version field of all images in manifest files to mach docker image tag
    * run kubectl apply on test cluster
    * run end to end tests
* tag the same commit used in the test stage with commit-hash/release
  * tag docker image (name = commit-hash/release)
    * push to production ecr
    * update version field of all images in manifest files to mach docker image tag
    * run kubectl apply on production cluster

## Notes

* What happends if merge into main branch happens again before the git tag stage? version fields of docker images in manifest files will not match the tested version of the code. Possible solution: use differnt git repositories for manifest files, with 3 type of branches: feature, rc, main. Only tagging the main branch in the code git repository triggers updating the version fileds in the main branch of the manifests repository. 