# CI/CD Logic using Git tags as triggers

## Sample architecture

* Frontend: static s3 website
* Backend: k8s service (lb)
* Database: dynamodb

## Git directory structure

* src
  * frontend
    * html
    * js
    * css
  * backend
    * k8s manifests
    * code

## Git branch names logic

* main branch
* feature / bug-fix branches

## Notes

* all k8s manifests use latest image versions (version field never get updated)

