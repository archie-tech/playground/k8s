#!/usr/bin/env python3

import boto3
import argparse


CLIENT = boto3.client("ec2")


def parse_arguments():
    epilog = "Example: ec2-utils.py --action list"
    parser = argparse.ArgumentParser(
        description='This script manages k8s-demo ec2 instances.', epilog=epilog)
    parser.add_argument('--action', '-a',
                        type=str,
                        required=True,
                        choices=["list", "start", "stop"])
    args = parser.parse_args()
    return args


def get_name(tags):
    for tag in tags:
        if tag["Key"] == "Name":
            return tag["Value"]


def get_instance_list():
    instance_list = []
    response = CLIENT.describe_instances(
        Filters=[
            {'Name': 'tag:Application', 'Values': ['k8s-demo']}
        ]
    )
    # print(json.dumps(response, default=str))
    for reservation in response["Reservations"]:
        for instance in reservation["Instances"]:
            instance_list.append({
                "id": instance["InstanceId"],
                "name": get_name(instance["Tags"]),
                "state":  instance["State"]["Name"]
            })
    return instance_list


def print_instance_list(instance_list):
    for instance in instance_list:
        print(instance)


def start_instances(instance_list):
    instances = []
    for instance in instance_list:
        instances.append(instance["id"])
    response = CLIENT.start_instances(InstanceIds=instances)


def stop_instances(instance_list):
    instances = []
    for instance in instance_list:
        instances.append(instance["id"])
    response = CLIENT.stop_instances(InstanceIds=instances)


if __name__ == "__main__":
    args = parse_arguments()
    instance_list = get_instance_list()
    match args.action:
        case "list":
            print_instance_list(instance_list)
        case "start":
            start_instances(instance_list)
        case "top":
            stop_instances(instance_list)
        case _:
            print("unknown action: {}".format(args.action))
