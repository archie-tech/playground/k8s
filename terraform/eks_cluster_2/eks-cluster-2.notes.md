# Setup tasks

* Install Terraform
* Install kubectl
* Install eks

# Post provisioning tasks

* scripts/config-kubectl
* scripts/allow-web-console-access
* scripts/install-calico.sh
* https://docs.aws.amazon.com/eks/latest/userguide/aws-load-balancer-controller.html