terraform {
  required_version = ">= 1.0"
  backend "s3" {
    bucket  = "terraform.dev.arinamal.com"
    key     = "playground/eks_cluster_2/terraform.tfstate"
    region  = "eu-west-1"
    profile = "arinamal-dev"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.1"
    }
  }
}

provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile
  default_tags {
    tags = {
      Application = var.application
      Customer    = var.customer
      Environment = var.environment
      Creator     = "terraform"
    }
  }
}
