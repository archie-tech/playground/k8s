#!/bin/bash

export VERSION="v3.25.1"

kubectl create namespace tigera-operator
helm install calico projectcalico/tigera-operator --version $VERSION --namespace tigera-operator

# kubectl get all -n tigera-operator
# kubectl get all -n calico-system