#!/bin/bash

export REGION="eu-west-1"
export CLUSTER="example"
export ARN="arn:aws:iam::946161444602:role/OrganizationAccountAccessRole"
export GROUP="system:masters"

eksctl create iamidentitymapping --region $REGION --cluster $CLUSTER --arn $ARN --group $GROUP --no-duplicate-arns

eksctl get iamidentitymapping --region $REGION --cluster $CLUSTER