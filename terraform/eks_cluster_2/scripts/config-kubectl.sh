#!/bin/bash

export REGION="eu-west-1"
export CLUSTER="example"

aws eks --region $REGION update-kubeconfig --name $CLUSTER