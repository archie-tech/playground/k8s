# Learn Terraform - Provision an EKS Cluster

This repo is a companion repo to the [Provision an EKS Cluster tutorial](https://developer.hashicorp.com/terraform/tutorials/kubernetes/eks), containing
Terraform configuration files to provision an EKS cluster on AWS.

# Setup tasks

* After provisioning your cluster, you need to configure kubectl to interact with it.

aws eks --region $(terraform output -raw region) update-kubeconfig --name $(terraform output -raw cluster_name)

* Allow access to eks resources from the web console

kubectl edit configmap aws-auth -n kube-system
    - groups:
      - system:masters
      rolearn: arn:aws:iam::946161444602:role/OrganizationAccountAccessRole
      username: archie_admin

* Enable metrics server

kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml

* Update education-eks-odrOivbC-node to allow all inbound traffic from self