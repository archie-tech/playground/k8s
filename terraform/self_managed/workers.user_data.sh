#!/bin/bash -xe

# Redirect output to local log file
exec > >(tee /var/log/user-data.log | logger -t user-data -s 2>/dev/console) 2>&1

# update package index files
apt-get -y update

function config_my_kernel_modules {
    # Forwarding IPv4 and letting iptables see bridged traffic
    touch /etc/modules-load.d/k8s.conf
    echo "overlay" >>/etc/modules-load.d/k8s.conf
    echo "br_netfilter" >>/etc/modules-load.d/k8s.conf
    sudo modprobe overlay
    sudo modprobe br_netfilter
}

function config_my_sysctl {
    # sysctl params required by setup, params persist across reboots
    touch /etc/sysctl.d/k8s.conf
    echo "net.bridge.bridge-nf-call-iptables  = 1" >>/etc/sysctl.d/k8s.conf
    echo "net.bridge.bridge-nf-call-ip6tables = 1" >>/etc/sysctl.d/k8s.conf
    echo "net.ipv4.ip_forward                 = 1" >>/etc/sysctl.d/k8s.conf
    # Apply sysctl params without reboot
    sudo sysctl --system
}

function install_my_containerd {
    apt-get -y install containerd golang-github-pelletier-go-toml
    mkdir /etc/containerd
    containerd config default >/etc/containerd/config.toml
    # enabl systemd cgroup driver
    sed -i 's/[[:blank:]]*SystemdCgroup[[:blank:]]*=[[:blank:]]*false/SystemdCgroup = true/g' /etc/containerd/config.toml
    tomll /etc/containerd/config.toml
    systemctl restart containerd
}

function install_my_k8s {
    apt-get -y install apt-transport-https ca-certificates curl
    curl -fsSL https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-archive-keyring.gpg
    echo "deb [signed-by=/etc/apt/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" >/etc/apt/sources.list.d/kubernetes.list
    apt-get -y update
    apt-get -y install kubelet=1.24.0-00 kubeadm=1.24.0-00 kubectl=1.24.0-00
    apt-mark hold kubelet kubeadm kubectl
}

config_my_kernel_modules
config_my_sysctl
install_my_containerd
install_my_k8s
