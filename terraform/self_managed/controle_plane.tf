resource "aws_instance" "k8s_master" {
  ami                         = var.ami
  instance_type               = var.instance_type
  key_name                    = var.ec2_key_pair
  associate_public_ip_address = true
  subnet_id                   = aws_subnet.public.id
  vpc_security_group_ids      = [aws_security_group.k8s_master.id]
  iam_instance_profile        = aws_iam_instance_profile.k8s_master.name
  user_data_replace_on_change = false
  user_data = templatefile(
    "${path.module}/controle_plane.user_data.sh", {
      K8S_VERSION    = var.k8s_version,
      CALICO_VERSION = var.calico_version
    }
  )
  tags = {
    Name = "${var.application} ${var.customer} k8s_master"
  }
  lifecycle {
    create_before_destroy = true
  }
}


resource "aws_iam_role" "k8s_master" {
  name = lower("${var.application}_${var.customer}_k8s_master")
  assume_role_policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Action" : "sts:AssumeRole",
          "Principal" : {
            "Service" : "ec2.amazonaws.com"
          },
          "Effect" : "Allow",
        }
      ]
    }
  )
}

resource "aws_iam_instance_profile" "k8s_master" {
  name = lower("${var.application}_${var.customer}_k8s_master")
  role = aws_iam_role.k8s_master.name
}


resource "aws_iam_role_policy_attachment" "k8s_master_ssm" {
  role       = aws_iam_role.k8s_master.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

resource "aws_iam_policy" "k8s_master" {
  name = lower("${var.application}_${var.customer}_k8s_master")
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" = "Allow",
          "Action" : "s3:ListBucket",
          "Resource" : "arn:aws:s3:::${var.deployments_bucket}"
        },
        {
          "Effect" = "Allow",
          "Action" : [
            "s3:GetObject",
            "s3:PutObject"
          ],
          "Resource" : "arn:aws:s3:::${var.deployments_bucket}/*"
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "k8s_master" {
  role       = aws_iam_role.k8s_master.name
  policy_arn = aws_iam_policy.k8s_master.arn
}

# https://kubernetes.io/docs/reference/networking/ports-and-protocols/
resource "aws_security_group" "k8s_master" {
  name   = lower("${var.application}_${var.customer}_k8s_master")
  vpc_id = aws_vpc.main.id
  tags = {
    Name = "${var.application} ${var.customer} k8s_master"
  }
  ingress {
    description = "Kubernetes API server"
    from_port   = 6443
    to_port     = 6443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "etcd server client API"
    from_port   = 2379
    to_port     = 2380
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "Kubelet API"
    from_port   = 10250
    to_port     = 10250
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "kube-scheduler"
    from_port   = 10259
    to_port     = 10259
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "kube-controller-manager"
    from_port   = 10257
    to_port     = 10257
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

output "k8s_master_server_id" {
  value = aws_instance.k8s_master.id
}
