# CKA (Certified Kubernetes Administrator) exam

* The cloud guru course uses version 1.24. Lates version is is 1.27.

# Control plane server (master) notes

* make sure swap is disabled (or not configured at all)

# A Cloud Guru - EKS Basics

* eksctl create cluster --name dev --region us-east-1 --nodegroup-name standard-workers --node-type t3.medium --nodes 3 --nodes-min 1 --nodes-max 4 --managed
